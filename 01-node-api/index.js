const http = require('http')
const { createHttpTerminator } = require('http-terminator')

const port = process.env.PORT || 1919
const server = http.createServer(async function (req, res) {
    res.writeHead(200, {"Content-Type": "application/json", "X-Author": "pietrom"})
    res.write(JSON.stringify({ status: 'ok', now: now() }))
    res.end()
}).listen(port)

const httpTerminator = createHttpTerminator({
  server,
})

function now() {
  return new Date().toISOString()
}

function closed() {
  console.log('Server closed at', now())
}

async function close(signal) {
  console.log(`${signal} signal received at`, now());
  await httpTerminator.terminate()
}

process.on('SIGTERM', () => {
    close('SIGTERM').then(() => console.log('Server stopped at', now()))
})

process.on('SIGINT', () => {
    close('SIGINT').then(() => console.log('Server stopped at', now()))
})

console.log('HTTP server started on port', port, 'at', now())
