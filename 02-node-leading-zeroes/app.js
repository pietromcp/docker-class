// app.js

function range(first, last) {
 const length = last - first + 1
 return new Array(length).fill(-1, 0, length).map(function(v, i) { return i + first; })
}

function factZeros(n) {
 return range(1, Math.floor(Math.log(n) / Math.log(5))).map(x => Math.pow(5, x)).map(x => Math.floor(n / x)).reduce((acc, curr) => acc + curr, 0)
}

// console.log(process.argv)

const n = process.argv.slice(2)[0]

console.log(factZeros(n))
