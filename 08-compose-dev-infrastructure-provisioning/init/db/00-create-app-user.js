db.createUser({
  user: "app",
  pwd: "apppwd",
  roles: [
    { role: "dbOwner", db: "app" },
    { role: "dbOwner", db: "appstat" },
    { role: "dbOwner", db: "appintegtest" }
  ]
});
